#! /usr/bin/bash
#
# Provisioning script common for all servers

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

#
# Variables
#

# Location (on the VM) of provisioning scripts and related files
provisioning_files=/vagrant/provisioning

#
# Package installation
#
#sudo yum install -y with contents of file "${provisioning_files}/common-packages" as argument
# cat "$(provisioning_files)/common-packages" | sudo yum install -y 
#sudo yum install -y ${provisioning_files}/common-packages 
sudo yum -y install $(cat ${provisioning_files}/common-packages | tr '\n' ' ')
#
# Admin user
#
sudo adduser koen
sudo gpasswd -a koen wheel

#sudo ssh-keygen -b 2048 -t rsa -f /tmp/sshkey -q -N ""
