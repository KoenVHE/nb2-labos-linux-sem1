## Laboverslag: 00

- Naam cursist: Koen Van Hessche
- Bitbucket repo: https://bitbucket.org/KoenVHE/nb2-labos-linux-sem1

### Procedures

1.1 SSH
Na installatie maak ssh sleutelpaar aan met commando:
ssh-keygen



Source:
https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html

1.2 Vagrant

Vagrant up srv010 ==> gaf een foutmelding ivm het aanmaken van de tweede netwerkadapter

Vagrant destroy ==> Door de bovenvermelde foutmelding poging om vm te verwijderen en daarna opnieuw te installeren
Vagrant is attempting to interface with the UI in a way that requires
a TTY. Most actions in Vagrant that require a TTY have configuration
switches to disable this requirement. Please do that or run Vagrant
with TTY.

vagrant destroy --force ==> gewoon vagrant destroy zorgde voor bovenvermelde melding; zeker dat ik alles wil verwijderen
==> srv010: Destroying VM and associated drives...
==> srv010: Running cleanup tasks for 'shell' provisioner...

vagrant up srv010 ==> installatie van machine; succesvol nu

vagrant ssh srv010 ==> inloggen op machine

sudo /vagrant/test/runbats.sh ==> het lopen van de tests
Dit liep niet goed tot het toevoegen vd map /srv010
1 test failed admin_user bert should exist

vim /vagrant/test/common ==> opent vim editor om variabele admin_user aan te passen naar koen

sudo yum install bash-completion, bind-utils, git, nano, tree, vim-enhanced, wget (-y)==> 
installatie van de packages bash-completion, bind-utils, git, nano, tree, vim-enhanced en
wget; de parameter -y om vooraf yes te antwoorden op het installeren van de packages

sudo adduser koen ==> toevoegen van user koen

yum>yellow dog update manager

sudo gpasswd -a username wheel ==> We can do this by adding the user to the wheel group (which gives sudo access to all of its members by default) through the gpasswd command.

ssh-keygen ==> ah proberen voor koen@192.168.15.10 no such luck na het uitvoeren chmod 600 koen@192.168.15.10/home/.ssh/authorized_keys
volgende foutmelding gekregen:
Could not chdir to home directory /home/koen: Permission denied
-bash: /home/koen/.bash_profile: Permission denied
-bash-4.2$
dit door per ongeluk chmod 600 op homedirectory
opgelost door vim /etc/sysconfig/selinux
mode=permissive

Opgelost nu terug mode=enforcing alles in orde

### Testplan en -rapport

Alle testen waren geslaagd
Weet niet goed wat hier in te vullen, zal volgende les vragen

### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

Het werken in linux sudo yum install bind-utils, git, etc...
-Toevoegen van gebruiker. 
-

#### Wat ging niet goed?

Wat lukte (eerst) niet? Welke problemen ben je tegengekomen (en hoe ben je tot een oplossing gekomen)? Wat hield je tegen om de opdracht tot een goed einde te brengen?

SSH toepassen op zowel git als om in te loggen op een andere machine koen@192.168.15.10 bvb.

#### Wat heb je geleerd?

Hoe ssh toe te passen
Zeker erop te letten dat je niet 

#### Waar heb je nog problemen mee?

/

### Referenties

Welke informatiebronnen heb je gebruikt bij het uitwerken van dit labo? Titels van boeken, URL's naar handleiding, HOWTO, enz.
https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html
Cursus en in de les vragen stellen 