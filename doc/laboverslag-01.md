## Laboverslag: 01

- Naam cursist: Koen Van Hessche
- Bitbucket repo: https://bitbucket.org/KoenVHE/nb2-labos-linux-sem1

### Procedures
Installatie lamp

firewall
systemctl enable firewalld
systemctl start firewalld

Apache
sudo yum install httpd
sudo systemctl start httpd.service

Mariadb
sudo yum install mariadb-server mariadb

sudo systemctl start mariadb

sudo systemctl enable mariadb.service


PHP
sudo yum install php php-mysql
sudo systemctl restart httpd.service


WORDPRESS
-------------
sudo mysql_secure_installation (deze gaan we moeten automatiseren)
(uitleg les hierover:
mariadb root wachtwoord instellen
(1)mysqladmin ==> doet hetzelfde is een commando
(2)mysql -u root -p$mariadb_root_password -e "SQLSTATEMENT" db-none ==> database met de naam test verwijderen
(3) Anonieme gebruiker verwijderen
(4) Database voor wordpress aanmaken
(5) gebruiker met permissies voor wordpress db

(4+5) Belangrijkste!!!
) 
------------------

mysql -u root -p

create database wordpress;
create user koen@localhost identified by '';
grant all priviliges on wordpress.* to wordpressuser@localhost identified by '';
flush priviliges;
exit
-------------------------------------------------

sudo yum install php-gd
sudo systemctl restart httpd.service

wget http://wordpress.org/latest.tar.gz

tar xzvf latest.tar.gz

sudo rsync -avP ~/wordpress/ /var/www/html/

mkdir /var/www/html/wp-content/uploads

sudo chown -R apache:apache /var/www/html/*


### Testplan en -rapport

Uitvoeren van lamp.bats
dit gebeurt na uitvoeren van: sudo /vagrant/test/runbats.sh

8 tests slagen...
----
✗ The necessary packages should be installed
   (in test file /vagrant/test/srv010/lamp.bats, line 24)
     `rpm -q wordpress' failed
   httpd-2.4.6-31.el7.centos.1.x86_64
   mariadb-server-5.5.44-1.el7_1.x86_64
   package wordpress is not installed
 ✓ The Apache service should be running
 ✗ The Apache service should be started at boot
   (in test file /vagrant/test/srv010/lamp.bats, line 34)
     `systemctl is-enabled httpd.service' failed
   disabled
 ✓ The MariaDB service should be running
 ✓ The MariaDB service should be started at boot
 ✓ The SELinux status should be ‘enforcing’
 ✗ Firewall: interface enp0s8 should be added to the public zone
   (in test file /vagrant/test/srv010/lamp.bats, line 50)
     `firewall-cmd --list-all | grep 'interfaces.*enp0s8'' failed
 ✗ Web traffic should pass through the firewall
   (in test file /vagrant/test/srv010/lamp.bats, line 54)
     `firewall-cmd --list-all | grep 'services.*http\b'' failed
 ✗ Mariadb should have a database for Wordpress
   (in test file /vagrant/test/srv010/lamp.bats, line 59)
     `mysql -uroot -p${mariadb_root_password} --execute 'show tables' ${wordpress_database}' failed
   ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)
 ✗ The MariaDB user should have "write access" to the database
   (in test file /vagrant/test/srv010/lamp.bats, line 63)
     `mysql -u${wordpress_user} -p${wordpress_password} \' failed
   ERROR 1045 (28000): Access denied for user 'wp_user'@'localhost' (using password: YES)
 ✓ The website should be accessible through HTTP
 ✗ The website should be accessible through HTTPS
   (in test file /vagrant/test/srv010/lamp.bats, line 82)
     `[ -n "$(ss -tln | grep '\b443\b')" ]' failed
 ✗ The certificate should not be the default one
   (in test file /vagrant/test/srv010/lamp.bats, line 89)
     `[ "0" -eq "${status}" ]' failed
 ✗ The Wordpress install page should be visible under http://192.168.15.10/wordpress/
   (in test file /vagrant/test/srv010/lamp.bats, line 98)
     `[ -n "$(curl --silent --location http://192.168.15.10/wordpress/ | grep '<title>WordPress')" ]' failed
 ✓ MariaDB should not have a test database
 ✗ MariaDB should not have anonymous users
   (in test file /vagrant/test/srv010/lamp.bats, line 107)
     `result=$(mysql -uroot -p${mariadb_root_password} --execute "select * from user where user='';" mysql)' failed
   ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)

16 tests, 10 failures



### Retrospectieve

Reflecteer na afwerken van het labo over volgende vragen en licht toe:

#### Wat ging goed?

De installatie van lamp

#### Wat ging niet goed?

werken met vagrant was moeilijker dan dat ik had ingeschat.
Voornamelijk het automatiseren van mysql -u root -p lukt moeizaam. 

#### Wat heb je geleerd?

Het leren werken met vagrant, achteraf gezien niet zo moeilijk om te verstaan. 

#### Waar heb je nog problemen mee?

Installatie van wordpress

### Referenties

https://mariadb.com/blog/installing-mariadb-10-centos-7-rhel-7
https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-centos-6
https://www.howtoforge.com/centos_wordpress_install


https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-on-centos-7